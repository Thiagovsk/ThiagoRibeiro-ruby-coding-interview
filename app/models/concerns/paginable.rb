# frozen_string_literal: true

module Paginable
  extend ActiveSupport::Concern

  included do
    #will return relation paginated (tweets in second page)
    #relation = Tweet.all
    #page = params[:page]
    #limit = 20 items
    def pagiantion(relation, page, limit)
      @page = page.to_i || 1
      @limit = limit || 20
      @relation = relation
      relation.limit(@limit).offset(@page * limit)
    end

    def current_page
    end

    def max_pages
      @max ||= (@relation.size / limit).ceil
    end
  end
end
